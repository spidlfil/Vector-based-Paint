//---------------------------------------------------------------------------

#ifndef fileHandlerH
#define fileHandlerH

#include "shape.h"
#include <vector>
#include "polygon.h"
#include "round.h"

class fileHandler{


public:
    fileHandler();
	bool loadObjects(std::vector<shape*> & allShapes);
	bool saveObjects(std::vector<shape*> & allShapes);
};

//---------------------------------------------------------------------------
#endif
