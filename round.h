//---------------------------------------------------------------------------

#ifndef roundH
#define roundH

#include "shape.h"




//virtual parent class for round shapes
class round : public shape {

	protected:
	std::pair<double, double> firstBound;

	//calculates hypertenuse with pythagoras theorem
	double hypotenuse(const std::pair<double,double> & oppositePoint) const;

	round(TPaintBox & belongsTo);

	public:
	bool virtual create(const double & offsetX, const double & offsetY, const double &zoomLevel) = 0;
};


//shape representing circle
class circle : public round {

protected:
	double radius;

public:
	circle(TPaintBox & belongsTo);

	bool virtual draw(const std::pair<double, double> & coordinate,
	const double & offsetX, const double & offsetY, const double &zoomLevel);
	bool virtual create(const double & offsetX, const double & offsetY, const double &zoomLevel);
	void virtual show(const std::pair<double, double> & coordinate,
	const double & offsetX, const double & offsetY, const double &zoomLevel);
	bool virtual writeToIni(TIniFile * IniFile, double numOfObjects) const;
};


//shape representing elipse
class elipse : public round {

	std::pair<double, double> secondBound;

public:
	elipse(TPaintBox & belongsTo);

	bool virtual draw(const std::pair<double, double> & coordinate,
	const double & offsetX, const double & offsetY, const double &zoomLevel);
	bool virtual create(const double & offsetX, const double & offsetY, const double &zoomLevel);
	void virtual show(const std::pair<double, double> & coordinate,
	const double & offsetX, const double & offsetY, const double &zoomLevel);
	bool virtual writeToIni(TIniFile * IniFile, double numOfObjects) const;

};

//---------------------------------------------------------------------------
#endif
