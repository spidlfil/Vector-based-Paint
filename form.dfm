object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 519
  ClientWidth = 787
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  OnClose = FormClose
  OnMouseWheelDown = FormMouseWheelDown
  OnMouseWheelUp = FormMouseWheelUp
  PixelsPerInch = 120
  TextHeight = 16
  object PaintBox1: TPaintBox
    Left = 0
    Top = 58
    Width = 787
    Height = 442
    Align = alClient
    OnMouseDown = PaintBox1MouseDown
    OnMouseMove = PaintBox1MouseMove
    OnMouseUp = PaintBox1MouseUp
    OnPaint = PaintBox1Paint
    ExplicitLeft = 44
    ExplicitTop = 24
    ExplicitWidth = 743
    ExplicitHeight = 464
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 787
    Height = 30
    Align = alTop
    Caption = 'Vector Paint'
    TabOrder = 0
  end
  object ToolBar1: TToolBar
    AlignWithMargins = True
    Left = 3
    Top = 33
    Width = 781
    Height = 22
    AutoSize = True
    ButtonWidth = 68
    Caption = 'ToolBar1'
    List = True
    AllowTextButtons = True
    TabOrder = 1
    object ToolButton2: TToolButton
      Left = 0
      Top = 0
      Action = Move
      AutoSize = True
      Style = tbsTextButton
    end
    object ToolButton4: TToolButton
      Left = 41
      Top = 0
      Action = Return
      Style = tbsTextButton
    end
    object ToolButton3: TToolButton
      Left = 90
      Top = 0
      Action = Rectangle
      AutoSize = True
      Style = tbsTextButton
    end
    object LineButton: TToolButton
      Left = 157
      Top = 0
      Action = Line
      AutoSize = True
      Style = tbsTextButton
    end
    object ToolButton5: TToolButton
      Left = 191
      Top = 0
      Action = Polygon
      Style = tbsTextButton
    end
    object ToolButton6: TToolButton
      Left = 246
      Top = 0
      Action = CircleAction
      Style = tbsTextButton
    end
    object ToolButton7: TToolButton
      Left = 289
      Top = 0
      Action = Ellipsis
      Style = tbsTextButton
    end
    object ToolButton8: TToolButton
      Left = 338
      Top = 0
      Action = Save
      Style = tbsTextButton
    end
    object ToolButton9: TToolButton
      Left = 377
      Top = 0
      Action = Load
      Style = tbsTextButton
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 500
    Width = 787
    Height = 19
    Panels = <
      item
        Text = 'Position and zoom'
        Width = 50
      end>
  end
  object SaveDialog1: TSaveDialog
    Left = 16
    Top = 296
  end
  object OpenDialog1: TOpenDialog
    Left = 16
    Top = 248
  end
  object ActionManager1: TActionManager
    Left = 8
    Top = 360
    StyleName = 'Platform Default'
    object CircleAction: TAction
      Caption = 'Circle'
      OnExecute = CircleActionExecute
    end
    object Rectangle: TAction
      Caption = 'Rectangle'
      OnExecute = RectangleExecute
    end
    object Line: TAction
      Caption = 'Line'
      OnExecute = LineExecute
    end
    object Polygon: TAction
      Caption = 'Polygon'
      OnExecute = PolygonExecute
    end
    object Ellipsis: TAction
      Caption = 'Ellipsis'
      OnExecute = EllipsisExecute
    end
    object Return: TAction
      Caption = 'Return'
      OnExecute = ReturnExecute
    end
    object Save: TAction
      Caption = 'Save'
      OnExecute = SaveExecute
    end
    object Load: TAction
      Caption = 'Load'
      OnExecute = LoadExecute
    end
    object Move: TAction
      Caption = 'Move'
      OnExecute = MoveExecute
    end
    object Red: TAction
      Caption = 'ColourRed'
      OnExecute = RedExecute
    end
    object Black: TAction
      Caption = 'ColourBlack'
      OnExecute = BlackExecute
    end
    object Blue: TAction
      Caption = 'ColourBlue'
      OnExecute = BlueExecute
    end
    object Green: TAction
      Caption = 'ColourGreen'
      OnExecute = GreenExecute
    end
    object Yellow: TAction
      Caption = 'ColourYellow'
      OnExecute = YellowExecute
    end
  end
  object MainMenu1: TMainMenu
    Left = 16
    Top = 416
    object File1: TMenuItem
      Caption = 'File'
      object Save1: TMenuItem
        Action = Save
      end
      object Load1: TMenuItem
        Action = Load
      end
    end
    object Shape1: TMenuItem
      Caption = 'Shape'
      object Ellipsis1: TMenuItem
        Action = Ellipsis
      end
      object Circle1: TMenuItem
        Action = CircleAction
      end
      object Line1: TMenuItem
        Action = Line
      end
      object Rectangle1: TMenuItem
        Action = Rectangle
      end
      object Polygon1: TMenuItem
        Action = Polygon
      end
    end
    object Movement1: TMenuItem
      Caption = 'Movement'
      object Move1: TMenuItem
        Action = Move
      end
      object Return1: TMenuItem
        Action = Return
      end
    end
    object Colour1: TMenuItem
      Caption = 'Colour'
      object Black1: TMenuItem
        Action = Black
        Caption = 'Black'
      end
      object ColourBlue1: TMenuItem
        Action = Blue
        Caption = 'Blue'
      end
      object ColourGreen1: TMenuItem
        Action = Green
        Caption = 'Green'
      end
      object ColourRed1: TMenuItem
        Action = Red
        Caption = 'Red'
      end
      object ColourYellow1: TMenuItem
        Action = Yellow
        Caption = 'Yellow'
      end
    end
  end
end
