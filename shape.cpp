//---------------------------------------------------------------------------

#pragma hdrstop

#include "shape.h"


shape::shape(TPaintBox& belongsTo): belongsTo(belongsTo)
{
}

void shape::show(const std::pair<double, double> & coordinate,
 const double & offsetX, const double & offsetY, const double &zoomLevel){
}

//if method does not have defined method to write INI file, it simply returns false
bool shape::writeToIni(TIniFile * IniFile, double numOfObjects) const{
    return false;
}

//---------------------------------------------------------------------------
#pragma package(smart_init)
