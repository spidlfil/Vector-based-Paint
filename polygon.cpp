//---------------------------------------------------------------------------

#pragma hdrstop

#include "polygon.h"
#include <stdlib.h>

//polygon methods
polygon::polygon(TPaintBox & entry): shape(entry), coordinates(), finished(false)
{
}

//creation of polygon once sufficient information was given
bool polygon::create(const double & offsetX, const double & offsetY, const double &zoomLevel)
{
	//called create with no points to connect
	if(coordinates.empty())
		return false;

	//starting coordinates
	double currX, currY, startX, startY;
	startX = coordinates[0].first + offsetX;
	startY = coordinates[0].second + offsetY;

	//makes line from current coordinate to next coordinate, then sets next coordinate as current and moves on
	for ( std::vector<std::pair<double,double> >::const_iterator it = coordinates.begin() ; it != coordinates.end(); it++){
		currX = it->first + offsetX;
		currY = it->second + offsetY;
		belongsTo.Canvas->MoveTo(startX*zoomLevel, startY*zoomLevel);
		belongsTo.Canvas->LineTo(currX*zoomLevel, currY*zoomLevel);
		startX = currX;
		startY = currY;
	}

	//connects last coordinate to first
	if(finished){
		belongsTo.Canvas->MoveTo(currX*zoomLevel, currY*zoomLevel);
		belongsTo.Canvas->LineTo((coordinates[0].first+offsetX)*zoomLevel, (coordinates[0].second+offsetY)*zoomLevel);

		//converts poins to propper format for Polygon() method
		TPoint * arr = new TPoint[coordinates.size()];
		for (uint8_t i = 0; i<coordinates.size(); i++)
			arr[i]=Point((coordinates[i].first+offsetX)*zoomLevel, (coordinates[i].second+offsetY)*zoomLevel);

		belongsTo.Canvas->Polygon(arr, coordinates.size()-1);
		delete[] arr;
	   }

   return true;
}


//rectangle methods
rectangle::rectangle(TPaintBox & entry): polygon(entry), middle(0,0)
{
}

//rectangle creation
bool rectangle::draw(const std::pair<double, double> & coordinate,
 const double & offsetX, const double & offsetY, const double &zoomLevel)
{

	//first point is the top right of rectangle
	//checks if default value is different
	if(middle == std::make_pair(0.0,0.0)){
		middle = coordinate;
		return true;
	}

	//if second point is given, it is treated as the opposite point of the first one
	//rectangle created between those two points
	double difX, difY;
	difX =coordinate.first - middle.first;
	difY = coordinate.second - middle.second ;


	coordinates.push_back(std::make_pair(middle.first/zoomLevel , middle.second/zoomLevel ));
	coordinates.push_back(std::make_pair(middle.first/zoomLevel + difX/zoomLevel, middle.second/zoomLevel));
	coordinates.push_back(std::make_pair(middle.first/zoomLevel + difX/zoomLevel, middle.second/zoomLevel + difY/zoomLevel));
	coordinates.push_back(std::make_pair(middle.first/zoomLevel , middle.second/zoomLevel + difY/zoomLevel));

	//points were created, now create the whole shape on drawing board
	finished = true;
	return false;

}


void rectangle::show(const std::pair<double, double> & coordinate,
 const double & offsetX, const double & offsetY, const double &zoomLevel)
{

	//first point is the top right of rectangle
	//checks if default value is different
	if((middle.first==0)&&(middle.second==0)){
		return ;
	}

	belongsTo.
	Canvas->
	Rectangle(middle.first+ offsetX, middle.second + offsetY,
	 coordinate.first + offsetX, coordinate.second + offsetY);
}

bool rectangle::writeToIni(TIniFile * IniFile, double numOfObjects) const
{

	//checks if rectangle is complete
	if(coordinates.size()!=4)
		return false;

	UnicodeString newSection = "shape_";
	newSection+=FormatFloat("0", numOfObjects);

	IniFile->WriteString(newSection, "type", "rectangle");
	IniFile->WriteInteger(newSection, "numOfPoints", 2);
	IniFile->WriteInteger(newSection, "X0", coordinates[0].first);
	IniFile->WriteInteger(newSection, "Y0", coordinates[0].second);
	IniFile->WriteInteger(newSection, "X1", coordinates[2].first);
	IniFile->WriteInteger(newSection, "Y1", coordinates[2].second);
	return true;
}




 //polygon methods, this one is not visible while we are creating it
genuinePolygon::genuinePolygon(TPaintBox & entry)   : polygon(entry)
{
}

//checks if given point is near start of this object
bool genuinePolygon::isNear(const std::pair<double, double> & coordinate,
 const double &zoomLevel) const{
	if(coordinates.empty())
		return false;


	//simple method of determining whether distance between points is smaller than
	//the arbitrary number of 20 - could be using pythagoras theorem, but isn't
	if(((abs(coordinates[0].first-coordinate.first/zoomLevel) +
		abs(coordinates[0].second-coordinate.second/zoomLevel)) <= 20)){
            return true;
		}


    return false;
}

//does not show polygon in making
bool genuinePolygon::draw(const std::pair<double, double> & coordinate,
 const double & offsetX, const double & offsetY, const double &zoomLevel)
{

	//point is close enough to start and shape can be created
	if(isNear(coordinate, zoomLevel)){
        finished = true;
		create( offsetX,  offsetY, zoomLevel);
		return false;
	}

	//points was not close enough, shape will be drawn further
    coordinates.push_back(coordinate);
	return true;
}

//line methods
line::line(TPaintBox & entry)   : polygon(entry)
{
}

//method for creating line
bool line::draw(const std::pair<double, double> & coordinate,
const double & offsetX, const double & offsetY, const double &zoomLevel)
{

	coordinates.push_back(std::make_pair(coordinate.first/zoomLevel,
	coordinate.second/zoomLevel));

	if(coordinates.size()>1){
		create( offsetX/zoomLevel,  offsetY/zoomLevel, zoomLevel);
        return false;
	}

	return true;
}

bool line::writeToIni(TIniFile * IniFile, double numOfObjects) const
{
	//line has to have two points exactly
	if(coordinates.size()!=2)
		return false;

	UnicodeString newSection = "shape_";
	newSection+=FormatFloat("0", numOfObjects);

	IniFile->WriteString(newSection, "type", "line");
	IniFile->WriteInteger(newSection, "numOfPoints", 2);
	IniFile->WriteInteger(newSection, "X0", coordinates[0].first);
	IniFile->WriteInteger(newSection, "Y0", coordinates[0].second);
	IniFile->WriteInteger(newSection, "X1", coordinates[1].first);
	IniFile->WriteInteger(newSection, "Y1", coordinates[1].second);
	return true;
}


//improved polygon methods
improvedPolygon::improvedPolygon(TPaintBox & entry)   : genuinePolygon(entry)
{
}


//method for polygon creation, shows current progress even before full polygon
// has been created
bool improvedPolygon::draw(const std::pair<double, double> & coordinate,
 const double & offsetX, const double & offsetY, const double &zoomLevel)
{


	if(isNear(coordinate, zoomLevel)){
        finished = true;
		create( offsetX/zoomLevel,  offsetY/zoomLevel, zoomLevel);
		return false;
	}

	if(coordinates.size()==0){
		coordinates.push_back(std::make_pair(coordinate.first/zoomLevel,
	coordinate.second/zoomLevel));
		return true;
	}



	double startX, startY;
	startX = coordinates.back().first/zoomLevel;
	startY = coordinates.back().second/zoomLevel;

	coordinates.push_back(std::make_pair(coordinate.first/zoomLevel,
	coordinate.second/zoomLevel));


	belongsTo.Canvas->MoveTo(startX+offsetX/zoomLevel, startY+offsetY/zoomLevel);
	belongsTo.Canvas->LineTo(coordinate.first+offsetX/zoomLevel,
	 coordinate.second+offsetY/zoomLevel);
	return true;
}

//not used
void improvedPolygon::show(const std::pair<double, double> & coordinate,
 const double & offsetX, const double & offsetY, const double &zoomLevel){

	create( offsetX/zoomLevel, offsetY/zoomLevel, zoomLevel);

}


bool improvedPolygon::writeToIni(TIniFile * IniFile, double numOfObjects) const{


	//polygon need to have at least two points
	if(coordinates.size()<=2)
		return false;

	UnicodeString newSection = "shape_";
	newSection+=FormatFloat("0", numOfObjects);

	IniFile->WriteString(newSection, "type", "polygon");
	IniFile->WriteInteger(newSection, "numOfPoints", (coordinates.size()+1));


	UnicodeString tempX = "X";
	UnicodeString tempY = "Y";
	UnicodeString tempHelper;

	double i;
	for(i = 0 ; i< coordinates.size(); i++){
		tempHelper = tempX +  FormatFloat("0", i);
		IniFile->WriteInteger(newSection, tempHelper, coordinates[i].first);
		tempHelper = tempY +  FormatFloat("0", i);
		IniFile->WriteInteger(newSection, tempHelper, coordinates[i].second);
	}

	tempHelper = tempX +  FormatFloat("0", i);
	IniFile->WriteInteger(newSection, tempHelper, coordinates[0].first);
	tempHelper = tempY +  FormatFloat("0", i);
	IniFile->WriteInteger(newSection, tempHelper, coordinates[0].second);

	return true;
}

//---------------------------------------------------------------------------
#pragma package(smart_init)