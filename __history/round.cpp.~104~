//---------------------------------------------------------------------------

#pragma hdrstop

#include "round.h"

//round shape methods
round::round(TPaintBox & entry): shape(entry), firstBound(0,0)
{
}


//pythagoras theorem, useful for finding radius
double round::hypotenuse(const std::pair<double,double> & oppositePoint) const{
	return sqrt((oppositePoint.first-firstBound.first)*(oppositePoint.first-firstBound.first)
	+ (oppositePoint.second-firstBound.second)*(oppositePoint.second-firstBound.second));

}

 //circle methods
circle::circle(TPaintBox & entry): round(entry)
{
}

//creates circle once enough parameters have been gathered
bool circle::create(const double & offsetX, const double & offsetY, const double &zoomLevel)
{

	if((firstBound.first==0)&&(firstBound.second == 0))
		return false;

	if(radius ==0)
		return false;



	belongsTo.Canvas->Ellipse((firstBound.first-radius+offsetX)*zoomLevel,
	 (firstBound.second-radius+offsetY)*zoomLevel,
	 (firstBound.first+radius+offsetX)*zoomLevel,
	 (firstBound.second+radius+offsetY)*zoomLevel);

	return true;
}


//gathers middle of circle and radius, then calls for its creation
bool circle::draw(const std::pair<double, double> & coordinate,
 const double & offsetX, const double & offsetY, const double &zoomLevel)
{
	if((firstBound.first==0)&&(firstBound.second == 0)){
		firstBound = coordinate;
		return true;
	}

	radius = hypotenuse(coordinate)/zoomLevel;

	//seems to work properly
	firstBound.first = firstBound.first/zoomLevel;
	firstBound.second = firstBound.second/zoomLevel;

	return false;
}

void circle::show(const std::pair<double, double> & coordinate,
 const double & offsetX, const double & offsetY, const double &zoomLevel)
{
	if((firstBound.first==0)&&(firstBound.second == 0))
		return;

	radius = hypotenuse(coordinate);

	if(radius ==0)
		return;

	belongsTo.Canvas->Ellipse((firstBound.first-radius+offsetX),
	 (firstBound.second-radius+offsetY),
	 (firstBound.first+radius+offsetX),
	 (firstBound.second+radius+offsetY));

}

bool circle::writeToIni(TIniFile * IniFile, double numOfObjects) const{


	/*
	ini->WriteInteger( "Form", "Top", Top );
    ini->WriteInteger( "Form", "Left", Left );
    ini->WriteString ( "Form", "Caption", Caption );
	ini->WriteBool   ( "Form", "InitMax", WindowState == wsMaximized );
	*/
	//todo
	std::string newSection = "shape_";
	newSection.append(FormatFloat("0", numOfObjects));

	IniFile->WriteString(newSection, "type", "circle")
	IniFile->writeInteger(newSection, "numOfPoints", 2)
	IniFile->writeInteger(newSection, "X1", firstBound.first)
	IniFile->writeInteger(newSection, "Y1", firstBound.second)
	IniFile->writeInteger(newSection, "X2", (firstBound.first-radius))
	IniFile->writeInteger(newSection, "Y2", firstBound.second)

	return true;
}



//eclipse methods
elipse::elipse(TPaintBox & entry): round(entry), secondBound(0,0)
{
}

//creates elipse once enough parameters have been gathered
bool elipse::create(const double & offsetX, const double & offsetY, const double &zoomLevel)
{

	if(firstBound == std::make_pair(0.0,0.0))
		return false;

	if(secondBound == std::make_pair(0.0,0.0))
		return false;

	//we are thinking of first point given as being top left, and second being
	//bottom right. This does not need to be the case, as the shape will be drawn
	//correctly even without it, but it makes it easier to imagine

	belongsTo.Canvas->Ellipse((firstBound.first+offsetX)*zoomLevel,
	(firstBound.second+offsetY)*zoomLevel,
	(secondBound.first+offsetX)*zoomLevel,
	(secondBound.second+offsetY)*zoomLevel);


	return true;
}

bool elipse::draw(const std::pair<double, double> & coordinate,
 const double & offsetX, const double & offsetY, const double &zoomLevel)
{
	//first point is the top right of rectangle
	//checks if default value is different
	if(firstBound == std::make_pair(0.0,0.0)){
		firstBound = coordinate;
		return true;
	}


	//seems to work properly
	firstBound.first = firstBound.first/zoomLevel;
	firstBound.second = firstBound.second/zoomLevel;

	//if second point is given, it is treated as the opposite point of the first one
	//elipse created between those two points
	secondBound = coordinate;
	secondBound.first = secondBound.first/zoomLevel;
	secondBound.second = secondBound.second/zoomLevel;
	return false;
}

void elipse::show(const std::pair<double, double> & coordinate,
 const double & offsetX, const double & offsetY, const double &zoomLevel)
{
	if((firstBound.first==0)&&(firstBound.second == 0))
		return;

	if((coordinate.first==0)&&(coordinate.second == 0))
		return;


	belongsTo.Canvas->Ellipse((firstBound.first+offsetX),
	(firstBound.second+offsetY),
	(coordinate.first+offsetX),
	(coordinate.second+offsetY));

}


bool elipse::writeToIni(TIniFile * IniFile, double numOfObjects) const{

	std::string newSection = "shape_";
	newSection.append(FormatFloat("0", numOfObjects));

	IniFile->WriteString(newSection, "type", "elipse")
	IniFile->writeInteger(newSection, "numOfPoints", 2)
	IniFile->writeInteger(newSection, "X1", firstBound.first)
	IniFile->writeInteger(newSection, "Y1", firstBound.second)
	IniFile->writeInteger(newSection, "X2", secondBound.first)
	IniFile->writeInteger(newSection, "Y2", secondBound.second)
	return true;
}






//---------------------------------------------------------------------------
#pragma package(smart_init)
