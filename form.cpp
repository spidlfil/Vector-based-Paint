//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "form.h"
#include <string>
#include "fileHandler.h"

//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------


void TForm1::checkCurrShape(){

	if(currShape->create(offsetX/zoomLevel, offsetY/zoomLevel, zoomLevel)){
		allShapes.push_back(currShape);
		currShape = NULL;
		}
}

void TForm1::refresh(){

	//TColor temp(PaintBox1->Canvas->Brush->Color);
	PaintBox1->Canvas->Brush->Color = clWhite;
	PaintBox1->Canvas->FillRect(ClientRect);
	PaintBox1->Canvas->Brush->Color = currColl;;

	for(unsigned int a = 0; a < allShapes.size();  a++)
		allShapes[a]->create(offsetX/zoomLevel, offsetY/zoomLevel, zoomLevel);
}


void TForm1::scrollMove(const TControl *Ctrl, const TPoint &MousePos){
	System::Types::TPoint center;

	double midX = PaintBox1->Width/2;
	double midY = PaintBox1->Height/2;

	// �f mouse is over paintbox
	if (Ctrl == (TControl *)PaintBox1){
		center = ScreenToClient(MousePos);
		midX -= center.X;
		midY -= center.Y;
		offsetX += midX/10/zoomLevel;
		offsetY += midY/10/zoomLevel;
		}
}

bool TForm1::createObject(TIniFile * IniFile, double i){

	//gets shape type
	UnicodeString getShape = "shape_";
	getShape += (FormatFloat("0", i));

	UnicodeString type = IniFile->ReadString(getShape,"type", "none");

	if(type=="none")
		return false;

	if(type=="circle")
		currShape = new circle(*PaintBox1);
	else if(type=="elipse")
		currShape = new elipse(*PaintBox1);
	else if(type=="rectangle")
		currShape = new rectangle(*PaintBox1);
	else if(type=="line")
		currShape = new line(*PaintBox1);
	else if(type=="polygon")
		currShape = new improvedPolygon(*PaintBox1);
	else{
		return false;
	}

	//draws shape as it were inserted manually
	double numPoints = IniFile->ReadInteger(getShape, "numOfPoints" , 0);
	double y;
	UnicodeString tempX = "X";
	UnicodeString tempY = "Y";
	UnicodeString tempHelper;
	std::pair<double, double> gotPoint;

	//goes over every point of the shape and adds it to object
	for(y = 0 ; y< numPoints; y++){
		tempHelper = tempX +  FormatFloat("0", y);
		gotPoint.first = IniFile->ReadInteger(getShape, tempHelper, 0);
		tempHelper = tempY +  FormatFloat("0", y);
		gotPoint.second = IniFile->ReadInteger(getShape, tempHelper, 0);
		if(!currShape->draw(gotPoint,offsetX, offsetY, zoomLevel)){
			allShapes.push_back(currShape);
			currShape = NULL;
			}
		}

	return true;
}

__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner), moving(false), willMove(false), zoomLevel(1), currColl(clBlack)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	ShowMessage("ahoj");
}
//---------------------------------------------------------------------------

void __fastcall TForm1::PaintBox1MouseMove(TObject *Sender, TShiftState Shift, int X,
		  int Y)
{

	//smoothly moves by dragging mouse over paintbox
	if(moving){
		offsetX += X-offsetHelperX;
		offsetY += Y-offsetHelperY;
		offsetHelperX = X;
		offsetHelperY = Y;
		refresh();
		return;
	}

	//shows x, y offset and zoomlevel, with default being 0,0,1
	StatusBar1->Panels->Items[0]->Text = FormatFloat("0", offsetX) + " " +
	 FormatFloat("0",offsetY)+ " " +
	 FormatFloat("0.0000",zoomLevel);

	if(!currShape)
		return;


	//some shapes get drawn before finishing, in case there is no curr shape
	//there is no need to refresh
	refresh();

   std::pair<double, double> coordinate(X-offsetX, Y-offsetY);
   //show possible shape
   currShape->show(coordinate, offsetX, offsetY, zoomLevel);
}
//---------------------------------------------------------------------------




void __fastcall TForm1::PaintBox1Paint(TObject *Sender){

    refresh();

	/*
	//refreshes
	if(currShape)
		currShape->create(offsetX, offsetY, zoomLevel);

	for(unsigned int a = 0; a < allShapes.size();  a++)
		allShapes[a]->create(offsetX, offsetY, zoomLevel);
		*/


    return;
}





void __fastcall TForm1::PaintBox1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y)
{

	if(moving){
		moving = false;
		return;
	}
  	//there is no shape set up
	if(!currShape)
		return;


	//make coordinate from current mouse position
   std::pair<double, double> coordinate(X-offsetX, Y-offsetY);

   //send coordinate to draw method, clear if shape has been finished
   if(!currShape->draw(coordinate,offsetX, offsetY, zoomLevel)){
		allShapes.push_back(currShape);
		currShape = NULL;
   }


}
//---------------------------------------------------------------------------


void __fastcall TForm1::FormMouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
		  bool &Handled)
{
	TControl *Ctrl =FindDragTarget(MousePos,False);
	scrollMove(Ctrl, MousePos);
	zoomLevel = zoomLevel * 0.9;
	refresh();

}
//---------------------------------------------------------------------------

void __fastcall TForm1::FormMouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
		  bool &Handled)
{
	TControl *Ctrl =FindDragTarget(MousePos,False);
	scrollMove(Ctrl, MousePos);
	zoomLevel = zoomLevel * 1.1;
	refresh();


}
//---------------------------------------------------------------------------

void __fastcall TForm1::PaintBox1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
          int X, int Y)
{
	if(willMove){
		willMove = false;
		moving = true;
		offsetHelperX = X;
		offsetHelperY = Y;
	}
}
//---------------------------------------------------------------------------





void __fastcall TForm1::CircleActionExecute(TObject *Sender)
{
	if(currShape)
		checkCurrShape();

	this->currShape = new circle(*PaintBox1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::MoveExecute(TObject *Sender)
{
   willMove = true;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RectangleExecute(TObject *Sender)
{
	if(currShape)
		checkCurrShape();

	this->currShape = new rectangle(*PaintBox1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LineExecute(TObject *Sender)
{
	if(currShape)
		checkCurrShape();

	this->currShape = new line(*PaintBox1);
}
//---------------------------------------------------------------------------


void __fastcall TForm1::PolygonExecute(TObject *Sender)
{
	if(currShape)
		checkCurrShape();

	this->currShape = new improvedPolygon(*PaintBox1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::EllipsisExecute(TObject *Sender)
{
	if(currShape)
		checkCurrShape();

	this->currShape = new elipse(*PaintBox1);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::ReturnExecute(TObject *Sender)
{
   offsetX =0;
   offsetY = 0;
   zoomLevel = 1;
   refresh();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::SaveExecute(TObject *Sender)
{
	//if no file was chosen, return
	if (SaveDialog1->Execute())
		IniFile = new TIniFile(SaveDialog1->FileName);
	 else
		return;

	double numOfObjects = 0;
	for(std::vector<shape*>::iterator it = allShapes.begin(); it != allShapes.end(); ++it) {
		if((*it)->writeToIni(IniFile, numOfObjects));
			numOfObjects++;
	}

	IniFile->WriteInteger("generalParameters", "numOfObjects", numOfObjects);
	IniFile->WriteInteger("generalParameters", "colour", currColl);

	delete(IniFile);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::LoadExecute(TObject *Sender)
{
	//if no file was chosen, return
	if (OpenDialog1->Execute())
		IniFile = new TIniFile(OpenDialog1->FileName);
	else
		return;

	if(!IniFile)
		return;


	//clearing up before applying new shapes
	for(unsigned int i = 0; i<allShapes.size(); i++)
		delete allShapes[i];

	allShapes.clear();

	offsetX= 0;
	offsetY = 0;
	zoomLevel = 1;

	double numOfObjects =0;
	if(IniFile)
		numOfObjects = IniFile->ReadInteger("generalParameters","numOfObjects", 0 );


	for(int i = 0; i<numOfObjects; i++){
		createObject(IniFile, i);
	}

	currColl = IniFile->ReadInteger("generalParameters", "colour", 0 );

	delete(IniFile);
	refresh();
}
//---------------------------------------------------------------------------


void __fastcall TForm1::FormClose(TObject *Sender, TCloseAction &Action)
{
	for(unsigned int i = 0; i<allShapes.size(); i++)
		delete allShapes[i];

	allShapes.clear();

	if(currShape)
		delete currShape;
}
//---------------------------------------------------------------------------

void __fastcall TForm1::RedExecute(TObject *Sender)
{
	currColl = clRed;
	refresh();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BlackExecute(TObject *Sender)
{
	currColl = clBlack;
	refresh();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::BlueExecute(TObject *Sender)
{
	currColl = clBlue;
	refresh();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::GreenExecute(TObject *Sender)
{
	currColl = clGreen;
	refresh();
}
//---------------------------------------------------------------------------

void __fastcall TForm1::YellowExecute(TObject *Sender)
{
	currColl = clYellow;
	refresh();
}
//---------------------------------------------------------------------------





