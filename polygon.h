//---------------------------------------------------------------------------

#ifndef polygonH
#define polygonH


#include "shape.h"

class polygon : public shape {

	protected:
		//shape can have many points connected
		std::vector<std::pair<double, double> > coordinates;

		//if polygon is finnished, it will be filled with colour
		bool finished;
		polygon(TPaintBox & belongsTo);

	public:
		bool virtual create(const double & offsetX, const double & offsetY, const double &zoomLevel);
};

//particular form of polygon with four points
class rectangle : public polygon {

	protected:
		std::pair<double, double> middle;

	public:
		rectangle(TPaintBox & current);

		//needs only two opposite points to be drawn
		bool virtual draw(const std::pair<double, double> & coordinate,
		const double & offsetX, const double & offsetY, const double &zoomLevel);
		void virtual show(const std::pair<double, double> & coordinate,
		const double & offsetX, const double & offsetY, const double &zoomLevel);
		bool virtual writeToIni(TIniFile * IniFile, double numOfObjects) const;
};

//helper class for creation of polygon with multiple points
class genuinePolygon : public polygon {

	protected:
		//checks if polygon can be closed
		bool isNear(const std::pair<double, double> & coordinate, const double &zoomLevel) const;

	public:
		genuinePolygon(TPaintBox & current);

		//draws until point isNear() to first point
		bool virtual draw(const std::pair<double, double> & coordinate,
		const double & offsetX, const double & offsetY, const double &zoomLevel);

};

//particular form of polygon with two points
class line: public polygon{

	public:
		line(TPaintBox & current);
		bool virtual draw(const std::pair<double, double> & coordinate,
		const double & offsetX, const double & offsetY, const double &zoomLevel);
		bool virtual writeToIni(TIniFile * IniFile, double numOfObjects) const;

};

//general polygon consisting of multiple points
//is actually used, contraty to "improvedPolygon" class
class improvedPolygon: public genuinePolygon{

	public:
		improvedPolygon(TPaintBox & current);

		//draws until point isNear() to first point
		bool virtual draw(const std::pair<double, double> & coordinate,
		const double & offsetX, const double & offsetY, const double &zoomLevel);

		//during creation we can see current outline drawn so far
		void virtual show(const std::pair<double, double> & coordinate,
		const double & offsetX, const double & offsetY, const double &zoomLevel);

		bool virtual writeToIni(TIniFile * IniFile, double numOfObjects) const;
};

//---------------------------------------------------------------------------
#endif


