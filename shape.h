//---------------------------------------------------------------------------

#ifndef shapeH
#define shapeH

#include "Vcl.ExtCtrls.hpp"
#include <vector>
#include <IniFiles.hpp>
#include <System.JSON.hpp>

class shape{

	protected:

	//keeps shape's paintbox where it will be drawn
	TPaintBox & belongsTo;
	shape(TPaintBox & belongsTo);

	public:

	//creates complete object, shows it
	bool virtual create(const double & offsetX, const double & offsetY,
	 const double &zoomLevel) = 0;

	//adds coordinate to incomplete object, eventually
	//draws it when enough parameters are added
	bool virtual draw(const std::pair<double, double> & coordinate,
	const double & offsetX, const double & offsetY, const double &zoomLevel) = 0;

	//shows how object would look if coordinate was to be added
	void virtual show(const std::pair<double, double> & coordinate,
	const double & offsetX, const double & offsetY, const double &zoomLevel);

	//writes shape to ini file
	bool virtual writeToIni(TIniFile * IniFile, double numOfObjects) const;

};

//---------------------------------------------------------------------------
#endif

