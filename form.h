//---------------------------------------------------------------------------

#ifndef formH
#define formH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ExtCtrls.hpp>
#include <System.ImageList.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.ImgList.hpp>
#include <Vcl.ToolWin.hpp>
#include "polygon.h"
#include "round.h"
#include "shape.h"
#include <IniFiles.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.Dialogs.hpp>
#include <System.Actions.hpp>
#include <Vcl.ActnList.hpp>
#include <Vcl.ActnMan.hpp>
#include <Vcl.PlatformDefaultStyleActnCtrls.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TPaintBox *PaintBox1;
	TPanel *Panel1;
	TToolBar *ToolBar1;
	TToolButton *ToolButton2;
	TToolButton *ToolButton3;
	TStatusBar *StatusBar1;
	TToolButton *LineButton;
	TToolButton *ToolButton5;
	TToolButton *ToolButton6;
	TToolButton *ToolButton7;
	TToolButton *ToolButton4;
	TToolButton *ToolButton8;
	TToolButton *ToolButton9;
	TSaveDialog *SaveDialog1;
	TOpenDialog *OpenDialog1;
	TActionManager *ActionManager1;
	TAction *CircleAction;
	TMainMenu *MainMenu1;
	TMenuItem *File1;
	TAction *Rectangle;
	TAction *Line;
	TAction *Polygon;
	TAction *Ellipsis;
	TAction *Return;
	TAction *Save;
	TAction *Load;
	TAction *Move;
	TMenuItem *Save1;
	TMenuItem *Load1;
	TMenuItem *Shape1;
	TMenuItem *Ellipsis1;
	TMenuItem *Polygon1;
	TMenuItem *Circle1;
	TMenuItem *Line1;
	TMenuItem *Rectangle1;
	TMenuItem *Movement1;
	TMenuItem *Move1;
	TMenuItem *Return1;
	TAction *Red;
	TAction *Black;
	TAction *Blue;
	TAction *Green;
	TAction *Yellow;
	TMenuItem *Colour1;
	TMenuItem *Black1;
	TMenuItem *ColourBlue1;
	TMenuItem *ColourGreen1;
	TMenuItem *ColourRed1;
	TMenuItem *ColourYellow1;
	void __fastcall Button1Click(TObject *Sender);
	void __fastcall PaintBox1MouseMove(TObject *Sender, TShiftState Shift, int X, int Y);
	void __fastcall PaintBox1Paint(TObject *Sender);
	void __fastcall PaintBox1MouseUp(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);
    	void __fastcall PaintBox1MouseDown(TObject *Sender, TMouseButton Button, TShiftState Shift,
		  int X, int Y);

	//zoom methods
	void __fastcall FormMouseWheelUp(TObject *Sender, TShiftState Shift, TPoint &MousePos,
		  bool &Handled);
	void __fastcall FormMouseWheelDown(TObject *Sender, TShiftState Shift, TPoint &MousePos,
		  bool &Handled);

	//actions for starting new shape cration
	void __fastcall CircleActionExecute(TObject *Sender);
	void __fastcall MoveExecute(TObject *Sender);
	void __fastcall RectangleExecute(TObject *Sender);
	void __fastcall LineExecute(TObject *Sender);
	void __fastcall PolygonExecute(TObject *Sender);
	void __fastcall EllipsisExecute(TObject *Sender);

	//returns to middle, resets offset and zoom level
	void __fastcall ReturnExecute(TObject *Sender);

	//file management actions
	void __fastcall SaveExecute(TObject *Sender);
	void __fastcall LoadExecute(TObject *Sender);

	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);

	//actions for colour change
	void __fastcall RedExecute(TObject *Sender);
	void __fastcall BlackExecute(TObject *Sender);
	void __fastcall BlueExecute(TObject *Sender);
	void __fastcall GreenExecute(TObject *Sender);
	void __fastcall YellowExecute(TObject *Sender);

private:	// User declarations

	//keeper of file for writing/reading
	TIniFile * IniFile;

	//keeps currently drawn shape
	shape * currShape;

	//keeps all already created shapes
	std::vector<shape*> allShapes;

	//variables for display changes
	double offsetHelperX, offsetHelperY;
	double offsetX, offsetY, zoomLevel;
	TColor currColl;

	//variables for movement action
	bool moving, willMove;

	//checks current shape for completeness, adds it to allShapes if it is complete
	void checkCurrShape();

	//clears PaintBox then creates all objects from allShapes
	void refresh();
	void scrollMove(const TControl *Ctrl, const TPoint &MousePos);
	bool createObject(TIniFile * IniFile, double i);

public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
